# Insira no core.php #
    Configure::write("Izzdb.explode", false); // para utilizar uma query por arquivo - ou coloque true para usar múltiplas queries por arquivo
    Configure::write("Izzdb.paths", array()); // caso vá utilizar mais de um path ou um path diferente para os arquivos de banco

# Insira no bootstrap.php #
    CakePlugin::load('Izzdb');

# Estrutura de pastas padrão #
    db
        dummy_data
            [datasource_name]
                [pasta com número do caso/card]
        functions
            [datasource_name]
        materialized views
            [datasource_name]
        procedures
            [datasource_name]
        skeleton
            [datasource_name]
        triggers
            [datasource_name]
        versions
            [datasource_name]
        views
            [datasource_name]
    
    *Exemplo de datasource_name: mysql, sqlserver, postgres e sqlite*

# Comandos já criados #
    init:                       executa todas as queries presentes na pasta skeleton (force explode implícito)
    views:                      executa todas as queries da pasta views
    procedures:                 executa todas as queries da pasta procedures
    functions:                  executa todas as queries da pasta functions
    triggers:                   executa todas as queries da pasta triggers
    materialized_views:         executa todas as queries da pasta materialized_views
    dummy_data [cardNumber]:    executa todas as queries da pasta dummy_data/númeroDoCard
    versions:                   executa todas as queries da pasta versions
    custom_folder [folderName]: executa todas as queries da pasta informada
    help:                       mostra todas as opções de comando
    default:                    se executado sem nenhum comando auxiliar, executa os seguintes comandos: views, procedures, functions, triggers e versions.
    force:                      você pode inserir esse parâmetro force no final dos demais comandos para burlar a checagem da tabela migrations

# Como usar: (dentro da pasta app pelo console) 
	Para executar o método default/main
		Console/cake Izzdb.sync_db

	Para executar outros comandos explicitados acima
		Console/cake Izzdb.sync_db comando

# Criando validações para poder executar certas queries #
	Caso deseje criar uma verificação prévia para que alguma query seja executada, basta criar um arquivo com o mesmo nome do anterior e adicionar um sufixo "_pq" (de pre-query) antes da extensão. A query do arquivo 1 só será executada se a query do arquivo 2 retornar count 0. 

	Exemplo:
		Arquivo 1: 1.0.2 - Adicionando novo modulo.sql
			INSERT INTO modulos (nome, link) VALUES ("Teste", "teste");
		Arquivo 2: 1.0.2 - Adicionando novo modulo_pq.sql
			SELECT * FROM modulos WHERE nome = "Teste" AND link = "teste";

# Configuração #NOT_EXPLODE #
    Caso deseje inserir ou executar um arquivo `_pq.sql` (sufixo) e o conteúdo conter `;` na query, deverá colocar na primeira linha o `#NOT_EXPLODE` para que seja rodada corretamente.
