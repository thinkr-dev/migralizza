CREATE TABLE IF NOT EXISTS migrations (
  id integer DEFAULT nextval('migration_seq'),
  database varchar(255) DEFAULT NULL,
  type varchar(255) DEFAULT NULL,
  filename varchar(255) DEFAULT NULL,
  affected_rows integer DEFAULT NULL,
  run_date timestamp DEFAULT current_timestamp,
  query text,
  error text,
  PRIMARY KEY (id)
);